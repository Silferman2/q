@extends('layouts.master')



@section ('content')

    <h1>Edit Ticket</h1>
    <br><br>

    <form action="/update/{{$ticket->id}}" method="POST" enctype="multipart/form-data">

        <label for="exampleFormControlInput1">Subject</label>
        <input class="form-control" id="exampleFormControlInput1" name="header" value="{{$ticket->header}}" required>

        <label for="exampleFormControlTextarea1">Description</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="12" name="text"
                  required>{{$ticket->text}}</textarea>


        <label for="inputAddress">Name</label>
        <input type="text" class="form-control" id="inputAddress" name="username" value="{{$ticket->username}}"
               required>

        <br>
        @if(sizeof($ticket->files)>0)
            <h5>Check to trash file</h5>
            @foreach($ticket->files as $file)
                <input type="checkbox" class="form-check-input" id="keepfile" name="keepfile[]" value="{{$file->id}}"
                       multiple>
                <label class="form-check-label" for="exampleCheck1">{{$file->filename}}</label><br>
            @endforeach
        @endif


        <div class="form-group col-md-4">
            <label for="inputZip">File</label>
            <input type="file" name="file[]" id="file" class="form-control" multiple>
        </div>


        <br>

        <input type="submit" value="Submit Ticket" name="submit" class="btn btn-primary">
        <input type="hidden" value="{{csrf_token() }}" name="_token">


    </form>

@endsection