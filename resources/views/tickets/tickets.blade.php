@extends('layouts.master')

@section ('content')

    <h1>{{ $titel }}</h1>
    <hr>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css"
          integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <link rel="stylecheet" href="./css/starter-template.css">
    @if($titel === 'Tickets')


        <div class="d-inline-flex p-2"><i class="fas fa-angle-double-up"></i></div>
        <a class="d-inline-flex p-2" href="/username/asc">Username</a>
        <a class="d-inline-flex p-2" href="/created/asc">Created</a>
        <a class="d-inline-flex p-2" href="/updated/asc">Updated</a>
        <div></div>
        <div class="d-inline-flex p-2"><i class="fas fa-angle-double-down"></i></div>
        <a class="d-inline-flex p-2" href="/username/desc">Username</a>
        <a class="d-inline-flex p-2" href="/created/desc">Created</a>
        <a class="d-inline-flex p-2" href="/updated/desc">Updated</a>
        <hr>
    @endif

    @forelse($tickets as $ticket)
        <h3 style="text-align: left">
            <a href="/{{$ticket->id}}">{{ $ticket->header }}</a>
        </h3>
        <div style="text-align: left">By: {{$ticket->username}}
            &nbsp; {{ \Carbon\Carbon::parse($ticket->created_at)->diffForHumans()}} </div>
        @if($ticket->created_at !==  $ticket->updated_at)
            <div style="text-align: left"> Updated:
                &nbsp; {{ \Carbon\Carbon::parse($ticket->updated_at)->diffForHumans()}} </div>
        @endif
        <br>

    @empty
        <p>
        <h1 class="display-1">Nothing to display</h1>
        </p>
    @endforelse
@endsection



