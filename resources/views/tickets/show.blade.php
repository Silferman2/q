@extends('layouts.master')



@section ('content')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.25/vue.min.js"></script>

    <form method="get" action="/">
        <button type="submit" class="close" aria-label="Close" onclick="">
            <span aria-hidden="true">&times;</span>
        </button>
    </form>
    <h1>Ticket</h1>

    <hr>

    <div class="container text-left bg-light text-secondary p-5">
        <h3>{{$ticket->header}}</h3>
        <h5>{{$ticket->text}}</h5>
        <div class="mt-5">Author: {{$ticket->username}}</div>
        <div>Created: {{$ticket->created_at}}</div>
        @if($ticket->created_at != $ticket->updated_at)
            <div>Updated: {{$ticket->updated_at}}</div>
        @endif
        <br>
        @foreach($ticket->files as $file)
            <a href="{{Storage::url($file->codedfilename)}}" download><i class="fas fa-paperclip">
                    &nbsp{{$file->filename}}</i></a>
        @endforeach

    </div>
    </div>


    @if(!$ticket->deleted_at)
        <span class="border">
    <a class="d-inline-flex p-2" href="/">See all</a>
    <a class="d-inline-flex p-2" href="{{$ticket->id}}/edit">Edit</a>
    <a class="d-inline-flex p-2" href="{{$ticket->id}}/trash">Throw away</a>
    </span>
        <hr>

        @include('tickets.comments')


    @else


        <h4>Deleted {{$ticket->deleted_at}}</h4>
        <hr>
        <span class="mb-5">
    <a class="d-inline-flex p-2" href="/trashcan">Back</a>
    <a class="d-inline-flex p-4" href="{{$ticket->id}}/delete">DELETE</a>
    <a class="d-inline-flex p-2" href="{{$ticket->id}}/restore">RESTORE</a>
    </span>

    @endif

@endsection