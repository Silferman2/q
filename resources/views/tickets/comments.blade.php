<!--
|   Simple stand alone comments "plug-in".
|   Using Firebase.
|   By: Andreas Hulman
-->


<div class="container mt-xl-5">
    <div class="d-flex align-items-center p-3 my-3 text-white-50  rounded "
         style="background-color: #3c3f41">
        <div class="">
            <h6 class=" text-white ">Comments</h6>
            <small class="text-info">For tickets</small>
        </div>
    </div>

    <div class="form-group">
        <textarea class="form-control" id="comment" rows="3" placeholder="Comment this Ticket" required></textarea>
        <input type="text" class="form-control" id="name" placeholder="Name" required>
        <button type="button" class="btn btn-secondary mt-3" onclick="addComment()">Add Comment</button>

    </div>
    <div class="my-3 p-3 bg-white rounded box-shadow">

        <div class="media text-muted pt-3">
        </div>
    </div>

</div>

<div class="container">
    <div id="postElement"></div>
</div>

<!-- -----------------------  JS comments ------------------------------------------>

<script src="https://www.gstatic.com/firebasejs/4.13.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.13.0/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.13.0/firebase.js"></script>




<script>
    // Initialize Firebase   Todo:  move to secure place
    var config = {
        apiKey: "AIzaSyDJ06uJ_3tr-5UpFByLevCFg4we3zJrjMU",
        authDomain: "comments-38afe.firebaseapp.com",
        databaseURL: "https://comments-38afe.firebaseio.com",
        projectId: "comments-38afe",
        storageBucket: "comments-38afe.appspot.com",
        messagingSenderId: "591407279309"
    };
    firebase.initializeApp(config);


    var database = firebase.database();

    var addComment = function () {
        var $name = document.getElementById('name').value;
        var $comment = document.getElementById('comment').value;
        if ($name !== '') {
            if ($comment !== '') {
                database.ref('{{$ticket->id}}').push({
                    username: $name,
                    comment: $comment
                });
            }
            //document.getElementById('name').value = '';
            document.getElementById('comment').value = '';
        }
    };

    // some HTML element on the page
    var postElement = document.getElementById("postElement");

    var addCommentElement = function (element, key, name, comment) {

        element.innerHTML +=
            '<div class="container-fluid border-top border-bottom" id=' + key + '>' +
            '<div class="row">' +
            '<div class="col-sm-9 word-wrap: break-word text-justify mt-3" id="comment"><blockquote><p>' + comment + '</p></blockquote></div>' +
            '<div class="col-sm-2 text-md-left align-middle mt-3" id="name"><i class="fas fa-user">&nbsp</i>' + name + '</div>' +
            '<div class="col-sm-1 align-middle mt-2"><a id=' + key + ' onclick="deleteComment(this)"  class="btn btn-primary "><i class="fas fa-trash-alt "></i></a></div>' +
            '</div>' +
            '</div>'
    };


    var deleteComment = function (tag) {
        firebase.database().ref('{{$ticket->id}}').child(tag.id).remove();
    }


    var commentsRef = firebase.database().ref('{{$ticket->id}}');

    commentsRef.on('child_added', function (data) {
        addCommentElement(postElement, data.key, data.val().username, data.val().comment);
    });


    commentsRef.on('child_removed', function (data) {
        var element = document.getElementById(data.key);
        element.outerHTML = "";
        delete element;
    });

</script>


