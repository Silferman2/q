@extends('layouts.master')

@section ('content')


    <h1>New Ticket</h1>
    <br><br>

        <form action="post" method="POST" enctype="multipart/form-data">

            <label for="exampleFormControlInput1">Subject</label>
            <input class="form-control" id="exampleFormControlInput1" name="header" style="font-size: xx-large" required>


            <label for="exampleFormControlTextarea1">Description</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="6" name="text" required style="font-size: xx-large"></textarea>


            <label for="inputAddress">Name</label>
            <input type="text" class="form-control" id="inputAddress" name="username" style="font-size: x-large" required>

        <br>

        <div class="form-group col-md-4">
            <label  for="inputZip">File</label>
            <input type="file" name="file[]"  multiple="true" id="file" class="form-control" >
        </div>


        <input type="submit" value="Submit Ticket" name="submit" class="btn btn-primary">
        <input type="hidden" value="{{ csrf_token() }}" name="_token">

    </form>


@endsection