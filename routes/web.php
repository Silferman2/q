<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;

Route::get('create','TicketsController@create');


Route::get('{sortby?}/{sorttype?}', 'TicketsController@index')->where([ 'sortby' => '[a-z]+', 'sorttype' => '[a-z]+']);


Route::post('post', 'TicketsController@store');


Route::get('{ticket}','TicketsController@show')->where([ 'ticket' => '[0-9]+']);


Route::get('{ticket}/edit', 'TicketsController@edit');


Route::post('update/{ticket}', 'TicketsController@update'); //fix route


Route::get('{ticket}/delete', 'TicketsController@delete');


Route::get('{ticket}/trash', 'TicketsController@trash');


Route::get('{ticket}/restore', 'TicketsController@restore');


Route::get('{ticket}/download', 'TicketsController@download');