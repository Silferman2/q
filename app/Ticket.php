<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Ticket extends Model
{

    use SoftDeletes;
    //  protected $guarded = [];
    protected $fillable = [
        'text',
        'header',
        'username'
    ];

    public function files()
    {

        return $this->hasMany(File::class);

    }
}
