<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use  Illuminate\Support\Facades\Input as Input;


class TicketsController extends Controller
{

    /**
     * @param string $orederdBy
     * @param string $asc_desc
     * @return mixed
     */
    public function index($orederdBy = 'created', $asc_desc = 'desc')
    {
        $listType = null;// Holds the name of the list type (Trash Can / Tickets)

        // if first parameter is trashcan then fetch all softdeleted tickets from db.
        if ($orederdBy == 'trashcan') {
            $tickets = Ticket::onlyTrashed()->get();
            $listType = 'Trash Can';

        }
        else // fetch Tickets in order
            {
            if ($orederdBy == 'updated') {
                $orederdBy = 'updated_at';
            } elseif ($orederdBy == 'created') {
                $orederdBy = 'created_at';
            }
            $tickets = Ticket::orderBy($orederdBy, $asc_desc)->get();
            $listType = 'Tickets';
        }

        return view('tickets.tickets', compact('tickets'))->withTitel($listType);
    }


    public function create()
    {

        return view('tickets.create');
    }


    public function store()
    {
        $ticket = new Ticket;
        $ticket->header = request('header');
        $ticket->text = request('text');
        $ticket->username = request('username');
        $ticket->save();
        $latestID = $ticket->id;

        // Stores files to diretory and DB
        if (request()->hasFile('file')) {
            foreach (request()->file as $_file) {

                $file = new \App\File;
                $file->ticket_id = $latestID;
                $file->filename = $_file->getClientOriginalName();
                $file->codedfilename = $_file->store('public');
                $file->save();
            }
        }

        session()->flash('message', "You created a new Ticket");
        return redirect('/' . $latestID);
    }


    public function show($id)
    {

        $ticket = \App\Ticket::withTrashed()->find($id);


        return view('tickets.show', compact('ticket'));
    }


    public function edit($id)
    {
        $ticket = \App\Ticket::find($id);


        return view('tickets.edit', compact('ticket'));
    }


    public function update($id)
    {

        $ticket = Ticket::find($id);

        $ticket->header = request('header');
        $ticket->text = request('text');
        $ticket->username = request('username');


        // Handling files set to be deleted from Ticket
        if (request()->trashfile) {
            foreach (request()->trashfile as $trash) {
                $tfile = \App\Ticket::find($id)->files()->find($trash);
                Storage::delete($tfile['codedfilename']);
                $tfile->delete();
            }
        }

        // Handling added files to the Ticket
        if (request()->hasFile('file')) {
            foreach (request()->file as $_file) {
                $file = new \App\File;
                $file->ticket_id = $id;
                $file->filename = $_file->getClientOriginalName();
                $file->codedfilename = $_file->store('public');
                $file->save();
            }
        }

        $ticket->save();
        session()->flash('message', 'Updated Ticket');
        return redirect('/' . $id);
    }


    public function delete($id)
    {

        $ticket = Ticket::onlyTrashed()->find($id);

        $files = $ticket->files()->get();

        // Fetch associated files and deletes.
        foreach ($files as $file) {

            Storage::delete($file['codedfilename']);
            $file->delete();
        }

        $ticket->forceDelete();

        session()->flash('message', 'Ticket deleted');

        return redirect('trashcan');
    }


    public function trash($id)
    {
        $ticket = Ticket::find($id);
        $ticket->delete();

        session()->flash('message', 'Ticket moved to Trash Can');
        return redirect('/');
    }


    public function restore($id)
    {
        $ticket = Ticket::onlyTrashed()->find($id);
        $ticket->restore();
        session()->flash('message', 'Ticket was restored');
        return redirect('trashcan');
    }
}
